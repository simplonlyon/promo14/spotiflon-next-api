export interface Artist {
    id: number;
    name: string;
    image: string;
  }
  
  export interface Albums {
    id: number;
    title: string;
    image: string;
    released: Date;
    artists: Artist[];
  }

 