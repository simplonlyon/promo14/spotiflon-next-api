import { GetStaticProps, NextPage } from "next";
import Link from "next/link";
import React, { useEffect } from "react";
import { Albums } from "../api/Models";
import axios from 'axios';



const AlbumList: NextPage<{ albums: Albums[] }> = ({ albums }) => {
  return (
    <div className="container">
     <h2 className="display text-center mt-2"> Liste albums</h2>
    
        <div className="row d-flex justify-content-between">
          {albums.map((album) => (
            <div className="col-md-3 mb-2" key={album.id}>
                <Link href={"/albums/"+album.id}>
                <div className="card table-hover">
                <img src={album.image} className="card-img-top " />
                <div className="card-body">
                  <p className="card-text">Titre: {album.title} </p>
                  <p className="card-text">Date de sortie: {album.released} </p>
                </div>
              </div>
                </Link>
             
            </div>
          ))}
        </div>
      </div>

  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const response = await axios.get('/api/album');
  return {
    props: {
      albums: response.data,
    },
  };
};

export default AlbumList;
