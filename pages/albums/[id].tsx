import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import React from "react";
import { Albums } from "../api/Models";
import axios from "axios";

interface Props {
  album: Albums;
}

const OneAlbum: NextPage<Props> = ({ album }) => {
  console.log("album " + album.artists[0].name);
  return (
    <div className="container">
      <div className="row ">
        <div className="row row-cols-1 row-cols-md-2 g-4">
          <div className="col">
            <div className="card">
              <img src={album.image} className="card-img-top" alt="..." />
              <div className="card-body">
                <h5 className="card-title">{album.id}</h5>
                <p className="card-text">
                  {album.title} {album.released}{" "}
                </p>
              </div>
            </div>
          </div>
          <div className="col">
            <div className="card">
              <ul className="list-group">
                <h3>Les artistes</h3>
                {album.artists.map((albumId) => {
                  return (
                    <li className="list-group-item" key={albumId.id}>
                      {albumId.name}
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const response = await axios.get<Albums[]>("/api/album");

  return {
    paths: response.data.map((item) => ({ params: { id: "" + item.id } })),
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async (context) => {
  try {
    const response = await axios.get("/api/album/" + context.params?.id);
    return {
      props: {
        album: response.data,
      },
    };
  } catch (e) {
    return {
      notFound: true,
    };
  }
};
export default OneAlbum;
